<?php /** @noinspection PhpHierarchyChecksInspection */
/** @noinspection PhpIllegalPsrClassPathInspection */

namespace tests;

use Codeception\Test\Unit;
use Exception;
use uhi67\mutex\SimpleMutex;
use UnitTester;
use Yii;
use yii\base\InvalidConfigException;
use yii\log\Logger;

class SimpleMutexTest extends Unit {
	/**
	 * @var UnitTester
	 */
	protected $tester;
	public $logFileName;

	/**
	 * Runs before each tests
	 * @throws InvalidConfigException
	 */
	protected function _before() {
		// Restore logger for real logging into file
		Yii::setLogger(Yii::createObject(Logger::class));
		Yii::$app->log->setLogger(Yii::getLogger());
		/** @noinspection PhpPossiblePolymorphicInvocationInspection */
		$this->logFileName = Yii::$app->log->targets[0]->logFile;
	}

	protected function _after() {
	}

	function testLog() {
		$I = $this->tester;
		// Empty logfile
		if(file_exists($this->logFileName)) unlink($this->logFileName);
		$logName = basename($this->logFileName);

		Yii::error('test error');
		Yii::warning('test warning');
		Yii::info('test info');

		Yii::getLogger()->flush(true);

		$I->amInPath(dirname($this->logFileName)); // Filesystem module added to unit suite yml
		$I->seeFileFound($logName);
		$I->openFile($logName);
		$I->seeInThisFile('test error');
		$I->seeInThisFile('test warning');
		$I->seeInThisFile('test info');

		$this->seeInLog('~\[error\]\[application\] test error\.*$~', 'See test error in logfile');
	}

	function testLock() {
		// Normal run
		$lock = new SimpleMutex(['key'=>'test1']);
		$status = $lock->lock();
		$this->assertEquals(0, $status);
		$fileName = Yii::getAlias('@runtime/mutex/'.md5('test1').'.lockfile');
		$this->assertFileNameEquals($fileName, $lock->fileName);
		$this->assertTrue(file_exists($fileName));
		$this->assertTrue(file_exists($fileName.'.dat'));
		$this->assertCount(2, $lock->data);
		$this->assertLessThanOrEqual(microtime(true), $lock->data[0]);

		$this->assertTrue($lock->unlock());
		$this->assertFalse(file_exists($fileName.'.dat'));

		$this->dontSeeInLog('~SimpleMutex[\w: \']+$~');
	}

	function testLock2() {
		// Occupied lock (simulation)
		$fileName = Yii::getAlias('@runtime/mutex/'.md5('test2').'.lockfile');
		file_put_contents($fileName, ($started = microtime(true)-0.5).',11111111');
		file_put_contents($fileName.'.dat', $started.',11111111');
		$file = fopen($fileName, 'w+');
		$this->assertTrue(flock($file, LOCK_EX | LOCK_NB));

		$lock = new SimpleMutex(['key'=>'test2', 'timeout'=>1000000]);
		$status = $lock->lock();
		$this->assertEquals(SimpleMutex::STATUS_RUNNING, $status, SimpleMutex::statusName($status));
		$this->assertCount(2, $lock->data);
		$this->assertEqualsWithDelta($started, $lock->data[0], 0.002);

		$this->dontSeeInLog('~SimpleMutex[\w: \']+$~');

		// unlock fake
		$this->assertTrue(flock($file, LOCK_UN));
	}

	function testLock3() {
		// Timed out lock (simulation)
		$fileName = Yii::getAlias('@runtime/mutex/'.md5('test3').'.lockfile');
		file_put_contents($fileName, ($started = microtime(true)-1.0).',223');
		file_put_contents($fileName.'.dat', (microtime(true)-1.0).',223');
		$file = fopen($fileName, 'w+');
		$this->assertTrue(flock($file, LOCK_EX | LOCK_NB));

		$lock = new SimpleMutex(['key'=>'test3', 'timeout'=>500000]);
		$status = $lock->lock();
		$this->assertEqualsWithDelta($started, $lock->started, 0.002);
		$this->assertEquals(SimpleMutex::STATUS_TIMEOUT, $status, SimpleMutex::statusName($status));

		// unlock fake
		$this->assertTrue(flock($file, LOCK_UN));

		$this->dontSeeInLog('~SimpleMutex[\w: \']+$~');
	}

	function testLock4() {
		// Timed out lock (simulation)
		$fileName = Yii::getAlias('@runtime/mutex/'.md5('test4').'.lockfile');
		file_put_contents($fileName, ($started = microtime(true)-1.0).',222222222');
		file_put_contents($fileName.'.dat', (microtime(true)-1.0).',222222222');
		$file = fopen($fileName, 'w+');
		$this->assertTrue(flock($file, LOCK_EX | LOCK_NB));

		$lock = new SimpleMutex(['key'=>'test4', 'timeout'=>5000]);
		$status = $lock->lock();
		$this->assertEqualsWithDelta($started, $lock->started, 0.002);
		$this->assertEquals(SimpleMutex::STATUS_STUCKED, $status);

		// unlock fake
		$this->assertTrue(flock($file, LOCK_UN));
		$this->dontSeeInLog('~SimpleMutex[\w: \']+$~');
	}

	function testRunWithLock() {
		// Single, succesful
		$status = SimpleMutex::runWithLock('test-5', function() { Yii::info('test5'); }, function($status, $params) {
			Yii::error('failed5 '.$status . ', '. json_encode($params));
		}, 1);

		$this->assertEquals(SimpleMutex::STATUS_SUCCESS, $status);
		$this->seeInLog('~test5~');
		$this->dontSeeInLog('~failed5[\w ]*$~');

		// Single, exception
		$status = SimpleMutex::runWithLock('test-6', function() { throw new Exception('test6'); }, function($status, $params) {
			Yii::error('failed6 '.SimpleMutex::statusName($status) . ', '. json_encode($params));
		}, 1);

		$this->assertEquals(SimpleMutex::STATUS_EXCEPTION, $status);
		$this->seeInLog('~test6~');
		$this->seeInLog('~failed6 exception, {"exception":{~');
	}

	function testExec() {
		$last = exec('php tests/_data/yii cron/test 1 test-exec 1000', $output);
		self::assertStringContainsString('test-exec success ', $last);
	}

	function testRunWithLockMulti() {
		// Empty logfile
		$logFileName = $this->logFileName;
		if(file_exists($logFileName)) unlink($logFileName);

		chdir(dirname(dirname(__DIR__)));
		/**
		 * 		   0...1...2...3...4...5...6...7 s
		 * test-7a [______] SUCCESS
		 * test-7b   | RUNNING
		 * test-7c      | TIMEOUT
		 * test-7d         [_X
		 * test-7e           | STUCKED
		 * test-7f            [__] SUCCESS
		 */
		// cron/test appends 3 lines into /tests/_output/mutextest.log: name started, name finished, name status
		$this->execInBackground('php tests/_data/yii cron/test 2 test-7a 3000000'); // parameters: wait name timeout -- STATUS_SUCCESS
		usleep(600*1000);
		$this->execInBackground('php tests/_data/yii cron/test 3 test-7b 3000000'); // STATUS_RUNNING
		usleep(600*1000);
		$this->execInBackground('php tests/_data/yii cron/test 3 test-7c 1000000'); // STATUS_TIMEOUT
		sleep(1);
		$this->execInBackground('php tests/_data/yii cron/test 2 test-7d 3000000'); // started, but will be killed
		usleep(500*1000);
		$this->execInBackground('php tests/_data/yii cron/test 2 test-7e 100000'); // STATUS_STUCKED
		usleep(200*1000);
		$this->execInBackground('php tests/_data/yii cron/test 1 test-7f 1000000'); // STATUS_SUCCESS
		usleep(1500*1000);

		$this->assertFileExists($logFileName);
		$this->tester->openFile($logFileName);
		$this->tester->seeInThisFile('test-7a started');
		$this->tester->seeInThisFile('test-7a finished');
		$this->tester->seeInThisFile('test-7a success');
		$this->tester->dontSeeInThisFile('test-7b started');
		$this->tester->seeInThisFile('test-7b running');
		$this->tester->dontSeeInThisFile('test-7c started');
		$this->tester->seeInThisFile('test-7c timeout');
		$this->tester->seeInThisFile('test-7d started');
		$this->tester->dontSeeInThisFile('test-7d finished');
		$this->tester->dontSeeInThisFile('test-7e started');
		$this->tester->seeInThisFile('test-7e stucked');
		$this->tester->seeInThisFile('test-7f started');
		$this->tester->seeInThisFile('test-7f finished');
		$this->tester->dontSeeInThisFile('lock error');
	}

	public function assertFileNameEquals($expected, $value, $message='') {
		$this->assertEquals(strtr($expected, '\\', '/'), strtr($value, '\\', '/'), $message);
	}

	public function dontSeeInLog($pattern, $message='') {
		Yii::getLogger()->flush(true);
		$f = fopen($this->logFileName, 'r');
		$this->assertFalse(!$f);
		$l = 0;
		$result = false;
		$mm = null;
		while(!feof($f)) {
			$line = trim(fgets($f));
			$l++;
			if(preg_match($pattern, $line, $mm)) {
				$result = true;
				break;
			}
		}
		fclose($f);
		$this->tester->expect("Pattern '$pattern' is NOT found in the log");
		$this->assertFalse($result, $message . ($mm ? " -- Pattern found in line $l: ".$mm[0] : ''));
	}

	public function SeeInLog($pattern, $message='') {
		Yii::getLogger()->flush(true);
		$f = fopen($this->logFileName, 'r');
		$this->assertFalse(!$f);
		$l = 0;
		$result = false;
		while(!feof($f)) {
			$line = fgets($f);
			$l++;
			if(preg_match($pattern, $line)) {
				$result = true;
				break;
			}
		}
		fclose($f);
		$this->tester->expect("Pattern '$pattern' is found in the log");
		if(!$message) $message = "Pattern '$pattern' is found in the log";
		$this->assertTrue($result, $message);
	}

	function execInBackground($cmd) {
		if (substr(php_uname(), 0, 7) == "Windows"){
			pclose(popen("start /B ". $cmd, "r"));
		}
		else {
			exec($cmd . " > /dev/null &");
		}
	}
}
