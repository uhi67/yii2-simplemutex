<?php /** @noinspection PhpIllegalPsrClassPathInspection */

/** @noinspection PhpUnused */

namespace app\commands;

use DateTime;
use tests\SimpleMutexTest;
use uhi67\mutex\SimpleMutex;
use Yii;

/**
 * #cron console command
 *
 * ### Usage:
 *
 * - crontab example
 *
 *  `   * * * * * php yii cron > /dev/null 2>&1`
 *
 */
class CronCommand extends AppCommandController {
	public $verbose;

	public function options($actionID) {
		return ['verbose'];
	}

	/**
	 * Cron job for testing SimpleMutex
	 *
	 * @see SimpleMutexTest
	 * @param int $wait
	 * @param string $name
	 * @param int $timeout
	 *
	 * @return int
	 */
	public function actionTest($wait, $name, $timeout) {
		$logFileName = Yii::$app->log->targets[0]->logFile; // dirname(__DIR__).'/tests/_output/mutextest.log';
		$status = SimpleMutex::runWithLock('crontest',
			function() use($logFileName, $name, $wait) {
				// The task
				$now = (new DateTime())->format('H:i:s.u');
				file_put_contents($logFileName, "$name started $now\n", FILE_APPEND);
				sleep($wait);
				$now = (new DateTime())->format('H:i:s.u');
				file_put_contents($logFileName, "$name finished $now\n", FILE_APPEND);
			},
			function($status, $params) use($name, $logFileName) {
				// Error handler
				$statusName = SimpleMutex::statusName($status);
				$now = (new DateTime())->format('H:i:s.u');
				$ex = isset($params['exception']) ? $params['exception']->getMessage() : '';
				$message = "Error at $name: $statusName, $now. $ex\n";
				Yii::error($message);
				file_put_contents($logFileName, $message, FILE_APPEND);
		}, $timeout);
		$statusName = SimpleMutex::statusName($status);
		$now = (new DateTime())->format('H:i:s.u');
		$result = "$name $statusName $now\n";
		file_put_contents($logFileName, $result, FILE_APPEND);
		echo $result;
		return 0;
	}
}
