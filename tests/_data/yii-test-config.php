<?php

/**
 * Application configuration shared by all test types
 */
require_once 'components/AppCommandController.php';
require_once 'components/CronCommand.php';

use yii\web\User;

return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__).'/',
	'controllerMap' =>  [
		'cron' => 'app\commands\CronCommand'
	],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
		'@runtime' => '@app/_output'
    ],
	'language' => 'hu',
	'timeZone' => 'Europe/Budapest',
	'bootstrap' => [
		'log',
	],
    'components' => [
        'mailer' => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
        'user' => [
			'class' => User::class,
			'identityClass' => 'app\models\AppUser',
			'enableSession' => true,  // default
			'enableAutoLogin' => true,
			'loginUrl' => ['site/mustlogin'],
        ],
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'logFile' => dirname(__DIR__).'/_output/mutextest.log',
					'levels' => ['error', 'warning', 'info'],
				],
			],
		],
    ],
];
