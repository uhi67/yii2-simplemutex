SimpleMutex for Yii2
====================
A simple flock-based implementation of mutex with timeout handling

Lock will be released in following cases:
- the owner process calls unlock
- when this lock object gets deleted
- when request or script ends
- when timeout*4 is exceeded and a subsequent call kills the process
- container initialization (composer install) purges the remained lock files and processes

Creates a file named `runtime/mutex/$key.lockfile` to to lock.
Creates also a file named `runtime/mutex/$key.lockfile.dat` containing "starttime, pid"

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

To install, either run
```
    composer require uhi67/yii2-simplemutex "*" 
```

or add

```
"uhi67/yii2-simplemutex" : "*"
```


Usage
-----

```php
		$lock = new uhi67\mutex\SimpleMutex(['key'=>'mylock');
		if($status = $lock->lock()) error("Locking failed with status ".uhi67\mutex\SimpleMutex::statusName($status));
		// Do your task here
		$lock->unlock();
		// Simple running any callable with lock, with timeout and failure handler
		/** @noinspection PhpUndefinedVariableInspection */
		uhi67\mutex\SimpleMutex::runWithLock('mylock2', $job, $fail, $timeout)
```
