<?php /** @noinspection PhpUnused */

namespace uhi67\mutex;

use Exception;
use Throwable;
use Yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;

/**
 * SimpleMutex for Yii2
 * ====================
 * A simple flock-based implementation of mutex with timeout handling
 * Lock will be released in following cases:
 * - the owner process calls unlock
 * - when this lock object gets deleted
 * - when request or script ends
 * - when timeout*4 is exceeded and a subsequent call kills the process
 * - container initialization (composer install) purges the remained lock files and processes
 *
 * Creates a file named `runtime/mutex/$key.lockfile` to to lock.
 * Creates also a file named `runtime/mutex/$key.lockfile.dat` containing "starttime, pid"
 *
 * Usage
 * -----
 * ```php
 * 		$lock = new SimpleMutex(['key'=>'mylock');
 * 		if($status = $lock->lock()) error("Locking failed with status ".SimpleMutex::statusName($status));
 * 		// Do your task here
 * 		$lock->unlock();
 *
 * 		// Simple running any callable with lock, with timeout and failure handler
 * 		SimpleMutex::runWithLock('mylock2', $job, $fail, $timeout)
 * ```
 * @author uhi
 * @property-read string $fileName
 * @property-read array $data -- runtime data of the locked resource [starttime, pid]
 **/
class SimpleMutex extends BaseObject {
	const
		STATUS_SUCCESS = 0,
		STATUS_RUNNING = 1,
		STATUS_TIMEOUT = 2,
		STATUS_LOCK_ERROR = 3,
		STATUS_EXCEPTION = 4,	// this process terminated with an exception
		STATUS_STUCKED = 5;		// previous process exceeded the dead limit

	public $key;  				// user given value
	/** @var int $timeout -- timeout in microseconds. Default is null -- no timeout used */
	public $timeout;
	/** @var int $stucktime -- time after previous process is may be killed upon collision. Default is timeout * 4. Specify false to disable. */
	public $stucktime;

	public $pid;
	/** @var float $started -- start timestamp of currently running instance (float seconds with microseconds) */
	public $started;
	/** @var Exception|Throwable $e -- cacthed exception (in kill method) */
	public $error;

	protected $file  = null;  	// resource to lock
	protected $own   = FALSE; 	// have we locked resource

	public static function statusNames() {
		return [
			static::STATUS_SUCCESS => 'success',
			static::STATUS_RUNNING => 'running',
			static::STATUS_TIMEOUT => 'timeout',
			static::STATUS_LOCK_ERROR => 'lock error',
			static::STATUS_EXCEPTION => 'exception',
			static::STATUS_STUCKED => 'stucked',
		];
	}

	public static function statusName($status) {
		/** @noinspection PhpUnhandledExceptionInspection */
		return ArrayHelper::getValue(static::statusNames(), $status, '');
	}

	public function init() {
		parent::init();
		//create a new resource or get exisiting with same key
		$this->file = fopen($this->fileName, 'w+');
		if($this->timeout && $this->stucktime===null) $this->stucktime = $this->timeout * 4;
	}


	public function __destruct() {
		if( $this->own == TRUE )
			$this->unlock( );
	}

	/**
	 * @return int -- STATUS_OK = 0 on success, other STATUS_XXX codes on failure
	 */
	public function lock() {
		if(!flock($this->file, LOCK_EX | LOCK_NB)) {
			// Crash and timeout handling
			$data = $this->data;
			if($data) {
				$this->started = (float)trim($data[0]);
				$this->pid = isset($data[1]) ? trim($data[1]) : false;

				$now = microtime(true);
				if($this->timeout && $this->started && ($this->started < ($now - $this->timeout/1000000.0)))  {
					if($this->stucktime && $this->started < ($now - $this->stucktime/1000000.0)) return self::STATUS_STUCKED;
					return self::STATUS_TIMEOUT;
				}
			}
			return self::STATUS_RUNNING;
		}

		ftruncate($this->file, 0);
		$this->pid = getmypid();
		$data = implode(',', [microtime(true), $this->pid]);
		fwrite( $this->file, $data."\n");
		fflush( $this->file);
		file_put_contents($this->fileName.'.dat', $data);
		$this->own = true;
		return self::STATUS_SUCCESS;
	}


	function unlock() {
		if($this->own) {
			if(!flock($this->file, LOCK_UN)) {
				Yii::error("SimpleMutex::unlock FAILED to release lock '$this->key'");
				return FALSE;
			}
			unlink($this->fileName.'.dat');
			ftruncate($this->file, 0); // truncate file
			fwrite($this->file, "\n");
			fflush($this->file);
			$this->own = FALSE;
		}
		else {
			Yii::error("SimpleMutex::unlock called on '$this->key' but it is not acquired by caller");
			return false;
		}
		return true;
	}

	/**
	 * @return bool -- success
	 */
	public function kill() {
		// kill stucked process
		if($this->pid) {
			$windows = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
			if($windows) exec("taskkill /PID $this->pid /F"); else exec("kill -9 $this->pid");
		}
		// Remove lock file
		try {
			flock($this->file, LOCK_UN);
			unlink($this->fileName) || !file_exists($this->fileName);
		}
		catch(Exception | Throwable $e) {
			Yii::error("SimpleMutex::kill failed on key $this->key, PID=$this->pid.".$e->getMessage());
			$this->error = $e;
			return false;
		}
		return true;
	}

	/**
	 * Deletes (all) stucked files (used at restart)
	 *
	 * @param string|array|null $key -- keyname or array of keynames or null for all lockfiles.
	 */
	public static function purge($key=null) {
		if(!$key) {
			$dh = opendir($dir = Yii::getAlias("@runtime/"));
	        while(($file = readdir($dh)) !== false) {
     		    if(filetype($dir . $file)=='file' && (substr($file, strrpos($file, '.')))=='lockfile') {
     		    	static::purge($dir . $file);
				}
        	}
        	closedir($dh);
			return;
		}

		if(is_array($key)) {
			foreach($key as $k) static::purge($k);
			return;
		}

		if(!file_exists($$key)) $fileName = Yii::getAlias("@runtime/$key.lockfile"); else $fileName = $key;
		if(file_exists($fileName)) {
			$lock = trim(file_get_contents($fileName)); // unlocked file is empty
			if($lock) {
				$lock = explode(',', trim(file_get_contents($fileName)));
				//$started = (int)trim($lock[0]);
				$pid = isset($lock[1]) ? trim($lock[1]) : false;
				if($pid) {
					$windows = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
					if($windows) exec("taskkill /PID $pid /F"); else exec("kill -9 $pid");
				}
			}
		}
		unlink($fileName) || !file_exists($fileName);
	}

	/**
	 * Calculates current lock filename based on key.
	 * Lock files are always in the `@runtime/mutex` directory. Creates if not exists.
	 *
	 * @return string
	 */
	public function getFileName() {
		$dir = Yii::getAlias("@runtime/mutex");
		if(!is_dir($dir)) mkdir($dir, 0774);
		$fileName = md5($this->key);
		return "$dir/$fileName.lockfile";
	}

	/**
	 * @return array|null -- [startTime, pid] or null if not locked (or sometime short after lock and before unlock)
	 */
	public function getData() {
		$dataFileName = $this->fileName.'.dat';
		if(!file_exists($dataFileName)) return null;
		return explode(',', trim(file_get_contents($dataFileName)));
	}

	/**
	 * A processzt lockolva futtatja.
	 * Ez ezért fontos, hogy cronból hívva ne érjék egymást utól.
	 * (A lockolási megoldás párhuzamos konténerekre csak akkor jó, ha a lock file közös fájlrendszerben van és támogatja a flockot.)
	 *
	 * Ha még fut az előző példány, és nem járt le az ideje, simán kihagyjuk a futást.
	 * Ha még fut az előző példány, és lejárt az ideje:
	 * 	- hibát naplózunk
	 * 	- futtatjuk a hibakezelő eljárást REASON_TIMEOUT státusszal (ez küld esetleg üzenetet valakinek)
	 *  - ha lejárt a türelmi idő is, akkor
	 * 		- a beragadt jobot (ha van töröljük)
	 * 		- a lockfájlt töröljük
	 * 		- hibát naplózunk
	 * 		- futtatjuk a hibakezelő eljárást a REASON_DEAD státusszal.
	 *
	 * fail meghívási minták
	 * - $fail(self::STATUS_EXCEPTION, ['exception'=>$e]);
	 * - $fail(self::STATUS_XXXX, ['started'=>$started]);
	 *
	 * @param string $name -- lock key
	 * @param callable $job
	 * @param callable|null $fail	-- meghívja, ha a lock elkérése sikertelen vagy ha a job exceptionra futott -- function(int $status, array $params): void
	 * @param int|null $timeout		-- figyelmeztetési határidő mikroszekundumban
	 * @param int|null|false $stucktime -- beszorulási határidő mikroszekundumban, ennek túllépése után a processzt az észlelő terminálhatja. Default értéke (null) a timeout négyszerese. False esetén nincs. Timeoutnál kisebb értékek esetén a timeout értéke lesz érvényes.
	 *
	 * @return int -- STATUS_OK on success or other STATUS_XXX code on failure
	 */
	public static function runWithLock($name, $job, $fail=null, $timeout=null, $stucktime=null) {
		$lock = new SimpleMutex(['key'=>$name, 'timeout'=>$timeout, 'stucktime'=>$stucktime]);
		if(!($status = $lock->lock())) {
			try {
				$job();
			}
			catch(Exception | Throwable $e) {
				Yii::error(["Cron: Exception at cron job '$name'", 'exception'=>$e->getMessage(), 'file'=>$e->getFile(), 'line'=>$e->getLine()]);
				Yii::info($e->getTraceAsString());
				if($fail!==null) $fail(self::STATUS_EXCEPTION, ['exception'=>$e]);
				return self::STATUS_EXCEPTION;
			}
			finally {
				$lock->unlock();
			}
		}
		else {
			$data = [
				'key'=>$lock->key,
				'pid'=>$lock->pid,
				'started' => $lock->started,
			];
			if($status == self::STATUS_STUCKED) {
				if(!$lock->kill()) {
					$data['message'] = 'kill failed';
				}
			}
			if($fail !== null) {
				if($lock->error) $data['exception'] = $lock->error;
				$fail($status, $data);
			}

			$startedx = date('Y:m:d H:i:s', $lock->started);
			$statusName = SimpleMutex::statusName($status);
			Yii::error("Lock '$name' $statusName, started='$startedx' timeout='$timeout', lockfile='$lock->file'");
		}
		return $status;
	}
}
